FROM python:3.10.2-alpine as base
WORKDIR /app
COPY business business
COPY external external
COPY main.py main.py
COPY requirements.txt requirements.txt
RUN apk add --no-cache openssl-dev libffi-dev build-base patchelf ccache && \
    python -m pip install --upgrade pip && \
    pip install -r requirements.txt && \
    pip install Nuitka ordered-set zstandard && \
    python -m nuitka \
        --include-module=business \
        --include-module=external \
        --follow-imports \
        --standalone \
        --onefile \
        --output-filename=backend \
        main.py

FROM alpine:3.17
WORKDIR /app
COPY --from=base /app/backend backend
CMD [ "/app/backend" ]
