from dataclasses import dataclass


@dataclass(slots=True, frozen=True)
class Dish:
    id: int = 0
    name: str = ''
