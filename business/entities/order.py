from dataclasses import dataclass
from datetime import datetime
from typing import TypeVar


@dataclass(slots=True, frozen=True)
class OrderDish:
    name: str = ''
    quantity: int = 0


@dataclass(slots=True, frozen=True)
class Order:
    id: int = 0
    table: int = 0
    created_at: datetime = datetime.now()
    dispatched_at: datetime = None
    dishes: tuple[OrderDish] = ()


class NoMoreOrdersAllowedException(Exception):
    def __init__(self, message="No more orders are allowed"):
        self.message = message
        super().__init__(self.message)


class NoOrderFoundException(Exception):
    def __init__(self, message="No order has found"):
        self.message = message
        super().__init__(self.message)