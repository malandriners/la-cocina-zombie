from business.entities.dish import Dish

def get_all_dishes(repo) -> list[Dish]:
    return repo.get_all()
