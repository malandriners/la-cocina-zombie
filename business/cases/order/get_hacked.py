from business.entities.order import Order, \
    NoMoreOrdersAllowedException

def get_hacked_orders(repo) -> list[Order]:
    return repo.get_hacked()
