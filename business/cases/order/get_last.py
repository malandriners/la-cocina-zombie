from business.entities.order import Order, \
    NoOrderFoundException

def get_last_order(repo) -> Order:
    item = repo.get_last()
    if not item:
        raise NoOrderFoundException('No se encontro ninguna comanda')

    return item
