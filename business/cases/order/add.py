from business.entities.order import Order, \
    NoMoreOrdersAllowedException

def add_order(repo, order: Order = None) -> Order:
    MAX_ORDERS = 5
    all_orders = repo.get_all()

    if len(all_orders) >= MAX_ORDERS:
        msg = 'Hasta que no se libere, no se aceptan más comandas'
        raise NoMoreOrdersAllowedException(f'Cocina llena! {msg}')

    order = repo.new(order)
    repo.log(order)
    repo.commit()

    return order
