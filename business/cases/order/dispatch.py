from business.entities.order import Order, \
    NoOrderFoundException
from .get_queue import get_order_queue


def dispatch_order(repo) -> Order:
    next = get_order_queue(repo)
    if not next:
        raise NoOrderFoundException('No hay ordenes para desparchar')
    
    item = repo.dispatch(next[0])
    repo.commit()

    return item

