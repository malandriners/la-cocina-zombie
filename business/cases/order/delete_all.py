from business.entities.order import Order

def delete_all_orders(repo)  -> bool:
    repo.delete_all()
    repo.commit()
    return True
