from business.entities.order import Order, \
    NoMoreOrdersAllowedException

def restore_hacked_orders(repo) -> int:
    num = repo.restore_hacked()
    if num:
        repo.commit()

    return num
