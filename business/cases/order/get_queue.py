from business.entities.order import Order

def get_order_queue(repo) -> list[Order]:
    def sort_key(item):
        esp = 10000 - sum(
            1 if x.name == 'ESPECIAL ZOMBIE' else 0
            for x in item.dishes
        )
        return f"{esp:05}_{item.created_at}"

    return sorted(repo.get_all(), key=sort_key)

