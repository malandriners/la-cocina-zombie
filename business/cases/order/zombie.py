from datetime import datetime, date
import requests


def attack(repo, order):
    new_date = get_hacked_date()
    if not new_date:
        return None
    
    new_created_at = datetime.combine(new_date, order.created_at.time())
    order = repo.zombie_hack(order, date)
    repo.commit()

    return None


def get_hacked_date() -> date:
    url = 'https://zombie-entrando-cocina.vercel.app/api/zombie/1'
    response = requests.get(url)
    
    if response.status_code != 200:
        return None
    data = response.json()
    if 'status' in data.keys():
        return None

    values = tuple(data.values())[0]
    clean_values = values.split('_')[-1].replace('$', '').split('-')
    print(clean_values)
    return date(**dict(zip(
        'year month day'.split(),
        tuple(int(x) for x in clean_values)
    )))
