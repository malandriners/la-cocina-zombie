from business.entities.order import Order

def get_all_orders(repo) -> list[Order]:
    return repo.get_all()
