import sys
sys.dont_write_bytecode = True

from external.settings import main_settings, rest_settings
import uvicorn

if __name__ == "__main__":
    uvicorn.run('external.rest.app:create_app',
        host=rest_settings.listen_host,
        port=rest_settings.listen_port,
        workers=rest_settings.workers,
        reload= main_settings.stage == 'dev',
        factory=True,
        server_header=False
        )
