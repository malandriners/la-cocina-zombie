FROM python:3.10.2-alpine as base
WORKDIR /app
COPY business business
COPY external external
COPY main.py main.py
COPY requirements.txt requirements.txt
RUN python -m pip install --upgrade pip && \
    pip install -r requirements.txt && \
    python -m compileall -b business && \
    python -m compileall -b external && \
    python -m compileall -b main.py && \
    find ./ -name "*.py" | xargs rm

FROM python:3.10.2-alpine
ENV STAGE=dev
ENV PYTHONDONTWRITEBYTECODE=-1
ENV PYTHONUNBUFFERED=1
WORKDIR /usr/src
COPY --from=base /root/.cache /root/.cache  
COPY --from=base /app/business ./business
COPY --from=base /app/external ./external
COPY --from=base /app/main.pyc ./
COPY requirements.txt ./
RUN pip install -r requirements.txt

CMD [ "python", "./main.pyc" ]