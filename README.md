#La Cocina Zombie

![backenddelasuerte2023](https://user-images.githubusercontent.com/1122071/223094282-d3db3828-60df-45d8-9723-acb41c4f8ec5.jpeg)

Edición del 2023 del desafío `El backend de la suerte` de `Web Reactiva`


## Desafio

Has abierto una Cocina para Zombies.

Los zombies están hartos de comer cerebros. No han ampliado su dieta desde hace siglos y las redes sociales les han empujado a querer tener una vida más moderna.

Quieren salir por ahí, tener vida social, divertirse sin riesgos.

Y tienen dinero en el bolsillo, siempre que tengan carne en la pierna, claro.

Tu misión va a ser crear un buen sistema para que cuando pidan su menú no se enfaden. Claro, cuando se enfadan ya sabes, te comen el cerebro...


Pinchar [aquí](https://github.com/webreactiva-devs/backend-de-la-suerte-2023) para ver más detalles del resto del desafío

## Solución
Se puede ver online desde [aquí](https://lacocinazombie.imanolvalero.com/docs)

### Objetivos personales
- Conocer el backend [PlanetScale](https://planetscale.com) que no conocía.
- Practicar las arquitecturas limpias
- Probar el analisis de seguridad de código que ofrece GitLab (SAST)
- Jugar con el CI de GitLab: [ver archivo](https://gitlab.com/malandriners/la-cocina-zombie/-/blob/main/.gitlab-ci.yml)
  - Pruebas de test que paran el flujo en caso de error
  - Reportes de tests automáticos en GitLab Pages: [ver informe](https://malandriners.gitlab.io/la-cocina-zombie/)
  - Jugar con las dependencias de los jobs
  - Lanzar jobs en paralelo
- Probar distintas formas de distribuir aplicaciones de Python en contenedores
  - Ejecutar el código fuente desde el interprete de Python: [ver Dockerfile](https://gitlab.com/malandriners/la-cocina-zombie/-/blob/main/source.Dockerfile)
  - Ejecutar el bytecode compilado desde el interprete de Python: [ver Dockerfile](https://gitlab.com/malandriners/la-cocina-zombie/-/blob/main/bytecode.Dockerfile)
  - Ejecutar el binario compilado con Nutika: [ver Dockerfile](https://gitlab.com/malandriners/la-cocina-zombie/-/blob/main/binary.Dockerfile)

  | Método | Tiempo | Tamaño en registro | Tamaño en servidor
  |:-|-:|-:|-:|
  | código fuente |  1'28" | 32.04 MiB | 73.91 Mib |
  | bytecode      |  1'30" | 29.70 MiB | 67.90 Mib |
  | binario       | 24'23" | 15.34 MiB | 18.98 Mib |

