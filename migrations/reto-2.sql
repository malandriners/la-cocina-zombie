alter table `orders` add column `table` int NOT NULL DEFAULT 0;

create table `order_dishes` (
    `id` int NOT NULL AUTO_INCREMENT,
    `order_id` int NOT NULL DEFAULT 0,
    `name` varchar(200) NOT NULL DEFAULT '',
    `quantity` int NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
);

create table `dishes` (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(200) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
);

insert into `dishes`
    (`name`)
values 
    ('Carita a la plancha'),
    ('Crujiente de cerebro en su salsa'),
    ('ESPECIAL ZOMBIE'),
    ('Fingers de merluza');

