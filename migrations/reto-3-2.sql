create table `order_logs` (
    `id` int not null auto_increment,
    `timestamp` timestamp not null default current_timestamp(),
    `order_id` int not null default 0,
    `number` int not null default 0,
    `prev` text not null default '',
    `new` text not null default '',
    primary key (`id`)
);
