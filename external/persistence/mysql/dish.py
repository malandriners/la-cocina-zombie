from dataclasses import replace
from business.entities.dish import Dish
from .base import MySQLBaseRepository


class MySQLDishRepository(MySQLBaseRepository):
    def map(self, data: dict):
        return replace(Dish(), **data)

    def get(self, dish_id: int) -> Dish:
        cur = self.get_conn().cursor()
        cur.execute('select * from `dishes` where `id`=%s;', (dish_id,))
        return self.map(cur.fetchone())

    def new(self, item: Dish):
        cur = self.get_conn().cursor()
        cur.execute(
            'insert into `dishes` (`id`, `name`) values (%s, %s);',
            (dish.id, dish.name)
        )
        return self.map(cur.fetchone())

    def get_all(self) -> list[Dish]:
        cur = self.get_conn().cursor()
        cur.execute('select * from `dishes`;')
        return [self.map(x) for x in cur.fetchall()]
