import pymysql
from pymysqlpool.pool import Pool
from . import MySQLOrderRepository, MySQLDishRepository


class MySQLPersistenceClient():
    def __init__(self, host, port, db, user,
                 passwd, ssl, pool_size, timeout) -> None:
        self.pool = Pool(
            host=host,
            port=port,
            db=db,
            user=user,
            passwd=passwd,
            ssl={'ssl': ssl},
            cursorclass=pymysql.cursors.DictCursor,
            charset='utf8',
            autocommit=False,
            min_size=1,
            max_size=pool_size,
            timeout=timeout,
            ping_check=True)
        self.pool.init()

    def close(self):
        self.pool.destroy()

    def get_conn(self):
        return self.pool.get_conn()

    def order_repository(self):
        return MySQLOrderRepository(self.pool)

    def dish_repository(self):
        return MySQLDishRepository(self.pool)
