import traceback
import json
from datetime import datetime
from dataclasses import replace, asdict
from business.entities.order import Order, OrderDish
from .base import MySQLBaseRepository
from .dish import MySQLDishRepository



class MySQLOrderRepository(MySQLBaseRepository):
    def mapOrderDish(self, data: dict):
        return replace(
            OrderDish(),
            **{k:v for k, v in data.items() if k in vars(OrderDish)}
        )


    def map(self, data: dict):
        order = replace(
            Order(),
            **{k:v for k, v in data.items() if k in vars(Order)}
        )
        if not order.dishes:
            dishes = self.get_dishes(order.id)
            order = replace(order, dishes=dishes)
        return order


    def get_dishes(self, order_id: int) -> tuple[OrderDish]:
        cur = self.get_conn().cursor()
        cur.execute(
            'select * from `order_dishes` where `order_id` = %s;',
            (order_id,)
        )
        return tuple(self.mapOrderDish(x) for x in cur.fetchall())


    def get(self, order_id: int):
        cur = self.get_conn().cursor()
        cur.execute('select * from `orders` where id = %s;', (order_id,))
        order = self.map(cur.fetchone())
        order = replace(order, dishes=self.get_dishes(order.id))
        return order


    def get_all(self) -> list[Order]:
        cur = self.get_conn().cursor()
        cur.execute('''select `order_dishes`.*
            from `order_dishes`
                left join `orders` on
                    `orders`.`id` = `order_dishes`.`order_id`
            where `orders`.`dispatched_at` is null;''')
        dishes = cur.fetchall()

        cur.execute('select * from `orders` where `dispatched_at` is null;')
        orders = cur.fetchall()
        for order in orders:
            order['dishes'] = tuple(
                self.mapOrderDish(dish) for dish in dishes 
                if dish['order_id'] == order['id']
            )
        
        return [self.map(x) for x in orders]


    def get_last(self) -> Order:
        cur = self.get_conn().cursor()
        cur.execute('''select coalesce(max(id), 0) as id 
            from orders where `dispatched_at` is null;'''
        )
        order = cur.fetchone()
        if not order['id']:
            return None

        return self.get(order['id'])


    def new(self, item: Order = None):
        cur = self.get_conn().cursor()
        new_item = None
        if item:
            cur.execute(
                'insert into `orders` (`table`) values (%s);',
                (item.table,)
            )
        else:
            cur.execute('insert into `orders` () values ();')
        cur.execute('select last_insert_id() as id;')
        new_id = cur.fetchone()

        new_item = self.get(new_id['id'])
        for dish in item.dishes:
            self.add_dish(new_item, dish.name, dish.quantity)

        return self.get(new_item.id)


    def add_dish(self, 
        order: Order, name:str, quantity:int
    ) -> OrderDish:
        cur = self.get_conn().cursor()
        existing = [
            x for x in self.get_dishes(order.id)
            if x.name == name
        ]
        if len(existing):
            cur.execute('''update `order_dishes` set 
                    `quantity` = `quantity` + %s
                where `name` = %s
                  and `order_id` = %s;''', 
                (quantity, name, order.id)
            )
            cur.execute('''select `quantity` from `order_dishes`
                where `name` = %s
                  and `order_id` = %s;''', 
                (name, order.id)
            )
            res = cur.fetchone()
            quantity = res['quantity']
        else:
            cur.execute('''insert into `order_dishes`
                    (`order_id`, `name`, `quantity`)
                    values (%s,%s,%s);''', 
                (order.id, name, quantity )
            )
        return OrderDish(name=name, quantity=quantity)


    def delete_all(self) -> Order:
        cur = self.get_conn().cursor()
        cur.execute('delete from orders;')
        cur.execute('delete from order_dishes;')
        cur.execute('delete from order_logs;')
        return True


    def clear_dishes(self, order_id: int) -> bool:
        cur = self.get_conn().cursor()
        cur.execute(
            'delete * from `order_dishes` where `order_id` = %s;',
            (order_id,)
        )
        return True


    def dispatch(self, item: Order) -> Order:
        cur = self.get_conn().cursor()
        res = None
        cur.execute(
            'update `orders` set `dispatched_at` = now() where id = %s',
            (item.id,)
        )

        return self.get(item.id)


    def log(self, item: Order) -> bool:
        cur = self.get_conn().cursor()
        res = None
        data = asdict(item)
        for field in 'created_at dispatched_at'.split():
            data[field] = data[field].isoformat() if data[field] else ''

        cur.execute(
            "insert `order_logs` (event, data) values ('created', %s)",
            (json.dumps(data),)
        )

        return True


    def zombie_hack(self, item: Order, date: datetime) -> list[Order]:
        cur = self.get_conn().cursor()
        res = None
        cur.execute(
            "update `orders` set created_at = %s where id = %s",
            (date, item.id)
        )

        return self.get(item.id)


    def _get_hacked(self) -> list:
        cur = self.get_conn().cursor()
        cur.execute('''
        select orders.*, tmp.orig_created_at
        from orders 
        left join (
            select
                id,
                timestamp(
                    SUBSTRING_INDEX(created_at,'T',1),
                    SUBSTRING_INDEX(created_at,'T',-1)
                ) as orig_created_at
            from (
                select 
                    json_extract(data, '$.id') as id,
                    replace(
                        json_extract(data, '$.created_at'),
                        '"',
                        ''
                    ) as created_at 
                from (
                    select cast(data as json) as data
                    from order_logs
                    where event = 'created'
                ) tmp_1
            ) tmp_0 
        ) tmp on
            tmp.id = orders.id
        where tmp.orig_created_at != orders.created_at''')
        return cur.fetchall()


    def get_hacked(self) -> list[Order]:
        return [self.map(x) for x in self._get_hacked()]


    def restore_hacked(self) -> int:
        data = self._get_hacked()
        if len(data): 
            cur = self.get_conn().cursor()
            for order in data:
                cur.execute(
                    'update `orders` set created_at = %s where id = %s',
                    (order['orig_created_at'], order['id'])
                )
        return len(data)
