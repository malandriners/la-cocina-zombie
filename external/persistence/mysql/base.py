from ..gateways.base import BaseRepository


class MySQLBaseRepository(BaseRepository):
    def __init__(self, pool):
        self.pool = pool
        self.conn = None

    def get_conn(self):
        if not self.conn:
            self.conn = self.pool.get_conn()
        return self.conn

    def close(self):
        if self.conn:
            if self.conn.open:
                self.conn.close()
            self.conn = None

    def commit(self):
        if self.conn:
            self.conn.commit()
        self.close()

    def rollback(self):
        if self.conn:
            self.conn.rollback()
        self.close()

    def __del__(self):
        self.close()
