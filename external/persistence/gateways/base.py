from abc import ABCMeta, abstractmethod
from importlib import import_module
from external.settings import main_settings
import external.persistence

class BaseRepository(metaclass=ABCMeta):
    @abstractmethod
    def get(cls, id: int):
        raise NotImplementedError

    @abstractmethod
    def new(cls, item):
        raise NotImplementedError

    @abstractmethod
    def get_all(cls) -> list:
        raise NotImplementedError
