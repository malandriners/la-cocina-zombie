from abc import abstractmethod
from business.entities.order import Order, OrderDish
from .base import BaseRepository


class OrderRepository(BaseRepository):
    @abstractmethod
    def new(self, item: Order) -> Order:
        raise NotImplementedError

    @abstractmethod
    def get(self, id: int) -> Order:
        raise NotImplementedError

    @abstractmethod
    def get_all(self) -> list[Order]:
        raise NotImplementedError

    @abstractmethod
    def get_last(self) -> Order:
        raise NotImplementedError

    @abstractmethod
    def delete_all(self) -> Order:
        raise NotImplementedError

    @abstractmethod
    def get_dishes(self, order_id: int) -> list[OrderDish]:
        raise NotImplementedError

    @abstractmethod
    def clear_dishes(self, order_id: int) -> bool:
        raise NotImplementedError

    @abstractmethod
    def add_dish(self, order: Order, name:str, quantity:int) -> OrderDish:
        raise NotImplementedError

    @abstractmethod
    def dispatch(self, item: Order) -> Order:
        raise NotImplementedError

    @abstractmethod
    def log(self, item: Order) -> Order:
        raise NotImplementedError

    @abstractmethod
    def get_hacked(self) -> list[Order]:
        raise NotImplementedError

    @abstractmethod
    def restore_hacked(self) -> int:
        raise NotImplementedError
