from external.settings import main_settings, rest_settings, mysql_settings
from external.persistence.mysql.client import MySQLPersistenceClient

def get_persistence_client():
    if main_settings.repository == 'mysql':
        return MySQLPersistenceClient(
            host=mysql_settings.host,
            port=mysql_settings.port,
            db=mysql_settings.database,
            user=mysql_settings.username,
            passwd=mysql_settings.password,
            ssl=mysql_settings.ssl,
            pool_size=mysql_settings.pool_size,
            timeout=mysql_settings.connect_timeout,
        )
    else: 
        return None