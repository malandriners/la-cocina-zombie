from fastapi import APIRouter, Depends
from business.cases.dish import get_all_dishes
from .schema import DishSchema, DishBaseListResponse
from ...persistence.gateways.dish import DishRepository

path = '/dishes'


def create_router(persistence_client):
    router = APIRouter()

    def get_repo():
        return persistence_client.dish_repository()

    @router.get(f'{path}', response_model=DishBaseListResponse)
    async def get_all_created_dishes(repo=Depends(get_repo)):
        res = DishBaseListResponse()

        try:
            res.result = [
                DishSchema.parse_entity(x)
                for x in get_all_dishes(repo)
            ]
            res.count = len(res.result)
            res.pages = 1
            res.page_size = len(res.result)
            res.current = 1
            
        except Exception as exc:
            res.error = str(exc.args)

        return res

    return router