from typing import TypeVar, Type
from pydantic import BaseModel
from business.entities.dish import Dish
from ...persistence.gateways.dish import DishRepository
from ..base.schema import BaseResponse, BaseListResponse


T = TypeVar('T', bound='DishSchema')

class DishSchema(BaseModel):
    id: int = 0
    name: str = ''

    def to_entity(self) -> Dish:
        return Dish(
            id=self.id,
            name=self.name,
        )

    @classmethod
    def parse_entity(cls: Type[T], item: Dish) -> T:
        return DishSchema(
            id=item.id,
            name=item.name,
        )


class DishBaseResponse(BaseResponse):
    result: DishSchema = None


class DishBaseListResponse(BaseListResponse):
    result: list[DishSchema] = []
