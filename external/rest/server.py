from os import killpg, getpid
from signal import SIGKILL
from contextlib import asynccontextmanager
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .base.routes import routers

class RestServer():

    def __init__(self, host, port, prefix, workers, persistence_client, stage):
        self.host = host
        self.port = port
        self.prefix = prefix
        self.workers = workers
        self.persistence_client = persistence_client
        self.stage = stage

        params = {'title': "La cocina zombie", 'redoc_url': None}
        if self.stage == 'prod':
            params.update({'docs_url': None})


        self.app = FastAPI(**params)

        self.app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

        @self.app.on_event("shutdown")
        async def shutdown():
            killpg(getpid(), SIGKILL)


        for path, router_factory in routers.items():
            self.app.include_router(
                router_factory(self.persistence_client),
                tags=[path],
                prefix=self.prefix
            )

    def start(self):
        res = uvicorn.run(self.app,
                    host=self.host,
                    port=self.port,
                    server_header=False)

