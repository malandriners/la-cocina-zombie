from ..order import path as order_path, create_router as create_order_router
from ..dish import path as dish_path, create_router as create_dish_router

routers = {
    order_path: create_order_router,
    dish_path: create_dish_router
}
