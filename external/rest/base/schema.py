import abc
from typing import Any
from pydantic import BaseModel
from external.persistence.gateways.base import BaseRepository


class BaseResponse(BaseModel):
    error: str = ''
    result: Any = None


class BaseListResponse(BaseResponse):
    count: int = 0
    pages: int = 0
    page_size: int = 0
    current: int = 0
    result: list[Any] = []


class BaseSchema(BaseModel, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def to_entity(self, repo: BaseRepository) -> Any:
        raise NotImplementedError

    @classmethod
    def parse_entity(cls, item: Any) -> Any:
        raise NotImplementedError
