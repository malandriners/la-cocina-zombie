from traceback import format_exc
from fastapi import APIRouter, Depends
from business.cases.order import add_order, delete_all_orders, \
    dispatch_order, get_all_orders, get_hacked_orders, get_last_order, \
    get_order_queue, restore_hacked_orders, zombie_attack
from .schema import OrderSchema, OrderResponse, OrderListResponse
from ..base.schema import BaseResponse
from ...persistence.gateways.order import OrderRepository


path = '/orders'


def create_router(persistence_client):
    router = APIRouter()

    def get_repo():
        return persistence_client.order_repository()

    @router.post(f'{path}/', response_model=OrderResponse)
    async def add_new_order(order: OrderSchema, repo=Depends(get_repo)):
        res = OrderResponse()
        try:
            res.result = OrderSchema.parse_entity(
                add_order(repo, order.to_entity())
            )

            hacked_item = zombie_attack(repo, res.result.to_entity())
            if hacked_item:
                res.error = (
                    f'Ataque zombie!'
                    f'Esta comanda ha sido alterada, '
                    f'pero nadie se ha enterado... aún'
                )
                res.result = OrderSchema.parse_entity(hacked_item)

        except Exception as exc:
            print(format_exc())
            res.error = str(exc.args[0])

        return res


    @router.get(f'{path}/last', response_model=OrderResponse)
    async def get_last_created_order(repo=Depends(get_repo)):
        res = OrderResponse()
        try:
            res.result = OrderSchema.parse_entity(get_last_order(repo))

        except Exception as exc:
            res.error = str(exc.args[0])

        return res


    @router.get(f'{path}', response_model=OrderListResponse)
    async def get_all_created_orders(repo=Depends(get_repo)):
        res = OrderListResponse()
        try:
            res.result = [
                OrderSchema.parse_entity(x) 
                for x in get_all_orders(repo)
            ]
            res.count = len(res.result)
            res.pages = 1
            res.page_size = len(res.result)
            res.current = 1

        except Exception as exc:
            res.error = str(exc.args[0])

        return res


    @router.delete(f'{path}', response_model=BaseResponse)
    async def delete_all_created_orders(repo=Depends(get_repo)):
        res = BaseResponse()
        try:
            res.result = delete_all_orders(repo)

        except Exception as exc:
            res.error = str(exc.args[0])

        return res


    @router.get(f'{path}/queue', response_model=OrderListResponse)
    async def get_order_dispatch_queue(repo=Depends(get_repo)):
        res = OrderListResponse()
        try:
            res.result = [
                OrderSchema.parse_entity(x) 
                for x in get_order_queue(repo)
            ]
            res.count = len(res.result)
            res.pages = 1
            res.page_size = len(res.result)
            res.current = 1
        except Exception as exc:
            res.error = str(exc.args[0])
        
        return res


    @router.put(f'{path}/dispatch', response_model=OrderResponse)
    async def dispatch_next_order(repo=Depends(get_repo)):
        res = OrderResponse()
        try:
            res.result = OrderSchema.parse_entity(dispatch_order(repo))

        except Exception as exc:
            res.error = str(exc.args[0])
        
        return res


    @router.get(f'{path}/hacked', response_model=OrderListResponse)
    async def get_the_orders_zombies_have_hacked(repo=Depends(get_repo)):
        res = OrderListResponse()
        try:
            res.result = [
                OrderSchema.parse_entity(x) 
                for x in get_hacked_orders(repo)
            ]
            res.count = len(res.result)
            res.pages = 1
            res.page_size = len(res.result)
            res.current = 1
        except Exception as exc:
            res.error = str(exc.args)
        
        return res
    

    @router.get(f'{path}/hacked/restore', response_model=BaseResponse)
    async def restore_the_orders_zombies_have_hacked(repo=Depends(get_repo)):
        res = BaseResponse()
        try:
            num = restore_hacked_orders(repo)
            if num == 0: 
                res.result = 'No había nada que restarurar'
            elif num == 1:
                res.result = 'Se ha restaurado 1 comanda hackeada'
            else:
                res.result = f'Se han restaurado {num} comandas hackeadas'
        except Exception as exc:
            print(format_exc())
            res.error = str(exc.args)
        
        return res

    return router
