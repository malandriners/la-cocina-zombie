from typing import Any
from datetime import datetime
from business.entities.order import Order, OrderDish
from ...persistence.gateways.order import OrderRepository
from ..base.schema import BaseSchema, BaseResponse, BaseListResponse
from ..dish.schema import DishSchema


class OrderDishSchema(BaseSchema):
    name: str = ''
    quantity: int = 0

    def to_entity(self) -> OrderDish:
        return OrderDish(
            name=self.name,
            quantity=self.quantity,
        )

    @classmethod
    def parse_entity(cls, item: OrderDish):
        return OrderDishSchema(
            name=item.name,
            quantity=item.quantity,
        )


class OrderSchema(BaseSchema):
    id: int = 0
    created_at: datetime = None
    dispatched_at: datetime = None
    table: int = 0
    dishes: list[OrderDishSchema]

    def to_entity(self):
        return Order(
            id=self.id,
            created_at=self.created_at,
            dispatched_at=self.dispatched_at,
            table=self.table,
            dishes=[x.to_entity() for x in self.dishes],
        )

    @classmethod
    def parse_entity(cls, item: Order):
        return OrderSchema(
            id=item.id,
            created_at=item.created_at,
            dispatched_at=item.dispatched_at,
            table=item.table,
            dishes=[OrderDishSchema.parse_entity(x) for x in item.dishes],
        )


class OrderResponse(BaseResponse):
    result: OrderSchema = None

class OrderListResponse(BaseListResponse):
    result: list[OrderSchema] = []
