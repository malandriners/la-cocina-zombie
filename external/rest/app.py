from os import killpg, getpid
from signal import SIGKILL
from contextlib import asynccontextmanager
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .base.routes import routers
from external.persistence.client import get_persistence_client
from external.settings import main_settings, rest_settings


def create_app():
    persistence_client = get_persistence_client()

    params = {'title': "La cocina zombie", 'redoc_url': None}
    if main_settings.stage == 'prod':
        params['docs_url'] = None

    app = FastAPI(**params)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.on_event("shutdown")
    async def shutdown():
        try:
            killpg(getpid(), SIGKILL)
        except: 
            pass

    for path, router_factory in routers.items():
        app.include_router(
            router_factory(persistence_client),
            tags=[rest_settings.prefix],
            prefix=rest_settings.prefix
        )

    return app
