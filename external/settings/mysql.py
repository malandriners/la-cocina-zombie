import os

class MySQLSettings():
    host: str = os.getenv('MYSQL_HOST') or 'localhost'
    port: int = int(os.getenv('MYSQL_PORT') or '3306')
    database: str = os.getenv('MYSQL_DATABASE') or 'test'
    username: str = os.getenv('MYSQL_USERNAME') or 'username'
    password: str = os.getenv('MYSQL_PASSWORD') or 'password'
    ssl: str = os.getenv('MYSQL_SSL') or '/etc/ssl/cert.pem'
    pool_size: int = int(os.getenv('MYSQL_POOL_SIZE') or '20')
    pool_recycle: int = int(os.getenv('MYSQL_POOL_RECYCLE') or '3600')
    pool_timeout: int = int(os.getenv('MYSQL_POOL_TIMEOUT') or '15')
    max_overflow: int = int(os.getenv('MYSQL_MAX_OVERFLOW') or '2')
    connect_timeout: int = int(os.getenv('MYSQL_CONNECT_TIMEOUT') or '60')

mysql_settings = MySQLSettings()
