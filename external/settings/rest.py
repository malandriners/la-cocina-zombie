import os

class RestSettings():
    prefix: str = '/api'
    listen_host: str = 'localhost'
    listen_port: int = 8080
    workers: int = 1

    def __init__(self):
        self.prefix = os.getenv('REST_PREFIX') or '/api'
        self.listen_host = os.getenv('REST_LISTEN_HOST') or 'localhost'
        self.listen_port = int(os.getenv('REST_LISTEN_PORT') or '8080')
        self.workers = 1 if os.getenv('STAGE') == 'dev' \
            else int(os.getenv('REST_WORKERS') or '20')

rest_settings = RestSettings()
