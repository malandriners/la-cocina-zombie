from dotenv import load_dotenv

load_dotenv()

from .main import main_settings
from .rest import rest_settings
from .mysql import mysql_settings
