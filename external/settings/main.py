import os

class MainSettings():
    stage: str = 'prod'
    repository: str = 'planetscale'

    def __init__(self):
        if os.getenv('STAGE').lower() != 'prod':
            self.stage = 'dev'
        self.repository = os.getenv('REPOSITORY') or 'sqlite'

main_settings = MainSettings()
