FROM python:3.10.2-alpine as base
WORKDIR /app
COPY business business
COPY external external
COPY main.py main.py
COPY requirements.txt requirements.txt

RUN /usr/local/bin/python -m pip install --upgrade pip && \
    pip install -r requirements.txt

FROM python:3.10.2-alpine
WORKDIR /usr/src
COPY --from=base /root/.cache /root/.cache  
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY business business
COPY external external
COPY main.py main.py
CMD [ "python", "./main.py" ]