from business.cases.order.zombie import get_hacked_date
from datetime import date
import requests
import pytest


def mock_requests_get(monkeypatch, status_code, json):
    def mock_get(uri, *args, **kwargs):
        mock = type('MockedReq', (), {})()
        mock.status_code = status_code
        mock.json = lambda: json
        obj = mock
        return obj
    monkeypatch.setattr(requests, 'get', mock_get)


def test_get_hacked_date_server_error(monkeypatch):
    mock_requests_get(monkeypatch, 400, {})
    assert get_hacked_date() is None


def test_get_hacked_date_not_attacked(monkeypatch):
    res = {"status":"Zombie pasa de largo"}
    mock_requests_get(monkeypatch, 200, res)
    assert get_hacked_date() is None


def test_get_hacked_date_attacked(monkeypatch):
    res = {"sw45sdf":"ZOMBIEEEEEEE____$$$$$$$$$$$$20$$$$$$$$$$23$$$$$$$$$$-0$$$$$$$$$$3-$$$$$$$$$$24$$$$$$$$$$$$$"}
    mock_requests_get(monkeypatch, 200, res)
    assert get_hacked_date() == date(2023, 3, 24)
